import React, { Component } from 'react'
import { Table, Button } from 'reactstrap';
import ModalForm from '../Modals/Modal'
import PopupConfirm from "../Modals/PopupConfirm"

class DataTable extends Component {

  deleteItem = id => {
      console.log(this.props);
      console.log(this.props.config);

    let confirmDelete = window.confirm('Delete item forever?')
    if(confirmDelete){
      fetch('http://localhost:4000/api/tables/delete', {
      method: 'post',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        id: id
      })
    })
      .then(response => response.json())
      .then(item => {
        this.props.deleteItemFromState(id)
      })
      .catch(err => console.log(err))
    }

  }

  handleChange = () => {
    // alert("test");
    let elems = document.getElementById("select-all");
    // console.log(elems)
    // console.log(elems.checked)
    if(elems.checked){
      let elems_2 = document.getElementsByClassName("select-item");
      
      for (var i = 0; i < elems_2.length; i++) {
        elems_2[i].checked = true;
      }
    }else{
      let elems_2 = document.getElementsByClassName("select-item");
      
      for (var i = 0; i < elems_2.length; i++) {
        elems_2[i].checked = false;
      }
    }
  }

  render() {
    let no = 0;
    const items = this.props.items.map(item => {
      // console.log(item);
      no += 1;
      // console.log(no);
      return (
        <tr key={item._id}>
          <th className="active"><PopupConfirm buttonLabel="Edit" item={item} updateState={this.props.updateState}/></th>
          <th scope="row">{no}</th>
          <td>{item.name}</td>
          <td>{item.category}</td>
          <td>{item.availability}</td>
          <td>{item.arrival}</td>
          <td>
            <div style={{width:"110px"}}>
              <ModalForm buttonLabel="Edit" item={item} updateState={this.props.updateState}/>
              {' '}
              <Button color="danger" onClick={() => this.deleteItem(item._id)}>Del</Button>
            </div>
          </td>
        </tr>
        )


      })

    return (
      <Table responsive hover>
        <thead>
          <tr>
            <th className="active"><input type="checkbox" className="select-all checkbox" id="select-all" name="select-all" onChange={this.handleChange} /></th>
            <th>No</th>
            <th>Name</th>
            <th>Category</th>
            <th>Availability</th>
            <th>Arrival</th>
          </tr>
        </thead>
        <tbody>
          {items}
        </tbody>
      </Table>
    )
  }
}

export default DataTable