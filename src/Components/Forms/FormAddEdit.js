import React from 'react';
import { Button, Form, FormGroup, Label, Input } from 'reactstrap';

class AddEditForm extends React.Component {
  state = {
    id: 0,
    name: '',
    category: '',
    availability: '',
    arrival: ''
  }

  onChange = e => {
    this.setState({[e.target.name]: e.target.value})
  }

  submitFormAdd = e => {
    e.preventDefault()
    fetch('http://localhost:4000/api/tables/add', {
      method: 'post',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        name: this.state.name,
        category: this.state.category,
        availability: this.state.availability,
        arrival: this.state.arrival
      })
    })
      .then(response => response.json())
      .then(item => {
        console.log(item);
        if(item.data != '') {
          let data = item.data;
          console.log(data);
          this.props.addItemToState(data)
          this.props.toggle()
        } else {
          console.log('failure')
        }
      })
      .catch(err => 
        console.log(err)
      )
  }

  submitFormEdit = e => {
    console.log("submitFormEdit");
    e.preventDefault()
    // console.log(this.state)
    // console.log(this.props.item)
    fetch("http://localhost:4000/api/tables/edit", {
      method: 'post',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        id: this.props.item._id,
        name: this.state.name,
        category: this.state.category,
        availability: this.state.availability,
        arrival: this.state.arrival
      })
    })
      .then(response => response.json())
      .then(item => {
        // console.log(item)
        if(item.data != "") {
          // console.log(item[0])
          let data = item.data;
          // console.log(data);
          this.props.updateState(data)
          this.props.toggle()
        } else {
          console.log('failure')
        }
      })
      .catch(err => console.log(err))
  }

  componentDidMount(){
    // if item exists, populate the state with proper data
    if(this.props.item){
      const { id, name, category, availability, arrival } = this.props.item
      this.setState({ id, name, category, availability, arrival })
    }
  }

  render() {
    return (
      <Form onSubmit={this.props.item ? this.submitFormEdit : this.submitFormAdd}>
        <FormGroup>
          <Label for="name">Name</Label>
          <Input type="text" name="name" id="name" onChange={this.onChange} value={this.state.name === null ? '' : this.state.name} />
        </FormGroup>
        <FormGroup>
          <Label for="category">Category</Label>
          <Input type="text" name="category" id="category" onChange={this.onChange} value={this.state.category === null ? '' : this.state.category}  />
        </FormGroup>
        <FormGroup>
          <Label for="availability">Availability</Label>
          <Input type="availability" name="availability" id="availability" onChange={this.onChange} value={this.state.availability === null ? '' : this.state.availability}  />
        </FormGroup>
        <FormGroup>
          <Label for="arrival">Arrival</Label>
          <Input type="text" name="arrival" id="arrival" onChange={this.onChange} value={this.state.arrival === null ? '' : this.state.arrival}  placeholder="" />
        </FormGroup>
        <Button>Submit</Button>
      </Form>
    );
  }
}

export default AddEditForm