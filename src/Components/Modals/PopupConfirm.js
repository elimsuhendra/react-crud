import React, { Component } from 'react'
import { Button, Modal, ModalHeader, ModalBody } from 'reactstrap'

class PopupConfirm extends Component {
  constructor(props) {
    super(props)
    this.state = {
      modal: false
    }
    this.count_selected = 0;
  }

  toggle = () => {
    this.setState(prevState => ({
      modal: !prevState.modal
    }))

    let elems = document.getElementsByClassName("select-item")
    // this.count_selected = 0;
    for (var i = 0; i < elems.length; i++) {
      console.log(elems[i].checked);
      if(elems[i].checked){
        this.count_selected += 1;
      }
    }
  }

  render() {
    console.log("modal render")
      const closeBtn = <button className="close" onClick={this.toggle}>&times;</button>

      const label = this.props.buttonLabel

      let button = ''
      let title = ''
      // console.log(this.props.item)
      if(label === 'Edit'){
        button = <input type="checkbox" className="select-item checkbox" id="select-item" name="select-item" onClick={this.toggle}/>
        title = this.count_selected+' table selected'
      } else {
        button = <Button
                  color="success"
                  onClick={this.toggle}
                  style={{float: "left", marginRight:"10px"}}>{label}
                </Button>
        title = 'Add New Item'
      }


      return (
      <div>
        {button}
        <Modal isOpen={this.state.modal} toggle={this.toggle} className={this.props.className}>
          <ModalHeader toggle={this.toggle} close={closeBtn}>{title}</ModalHeader>
          <ModalBody>
           
          </ModalBody>
        </Modal>
      </div>
    )
  }
}

export default PopupConfirm