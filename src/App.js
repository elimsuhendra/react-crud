import React, { Component } from 'react'
import { Container, Row, Col } from 'reactstrap'
import ModalForm from './Components/Modals/Modal'
import DataTable from './Components/Tables/DataTable'
import { CSVLink } from "react-csv"
import Config from "./Config"
import PopupConfirm from "./Components/Modals/PopupConfirm"

class App extends Component {
  state = {
    items: []
  }

  config = new Config();
  base_url = this.config.state.base_url

  getItems(){
    console.log(this.config);
    fetch(this.base_url+'/api/tables')
      .then(response => response.json())
      .then(items => this.setState({items}))
      .catch(err => console.log(err))
  }

  addItemToState = (item) => {
    console.log("addItemToState");
    this.setState(prevState => ({
      items: [...prevState.items, item]
    }))
  }

  updateState = (item) => {
    console.log("updateState");
    const itemIndex = this.state.items.findIndex(data => data.id === item.id)
    const newArray = [
    // destructure all items from beginning to the indexed item
      ...this.state.items.slice(0, itemIndex),
    // add the updated item to the array
      item,
    // add the rest of the items to the array from the index after the replaced item
      ...this.state.items.slice(itemIndex + 1)
    ]
    // console.log(newArray)
    this.setState({ items: newArray })
  }

  deleteItemFromState = (id) => {
    console.log("deleteItemFromState");
    console.log(id);
    console.log(this.state.items);
    // const updatedItems = this.state.items.filter(item => item.id !== id)
    let updatedItems = this.state.items.filter(function (item) {
      // console.log(item)
      // console.log(item._id, id)
      return item._id !== id 
    })
    console.log(updatedItems);
    this.setState({ items: updatedItems })
  }

  componentDidMount(){
    this.getItems()
  }

  render() {
    return (
      <Container className="App">
        <Row>
          <Col>
            <h1 style={{margin: "20px 0"}}>CRUD Database</h1>
          </Col>
        </Row>
        <Row>
          <Col>
            <DataTable items={this.state.items} updateState={this.updateState} deleteItemFromState={this.deleteItemFromState} />
          </Col>
        </Row>
        <Row>
          <Col>
            <CSVLink
              filename={"db.csv"}
              color="primary"
              style={{float: "left", marginRight: "10px"}}
              className="btn btn-primary"
              data={this.state.items}>
              Download CSV
            </CSVLink>
            <ModalForm buttonLabel="Add Item" addItemToState={this.addItemToState}/>
          </Col>
        </Row>
      </Container>
    )
  }
}

export default App